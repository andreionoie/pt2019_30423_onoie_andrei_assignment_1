package test;

import PolyLogic.PolyOperations;
import PolyLogic.Polynomial;
import PolyLogic.QRPair;
import org.junit.Test;

import static org.junit.Assert.*;

public class PolyOperationsTest {
    Polynomial p, q;

    @Test
    public void add() {
        initPolynomials();
        Polynomial r = PolyOperations.add(p, q);
        Polynomial tmp = new Polynomial();

        tmp.add(1, 3);
        tmp.add(-2, 2);
        tmp.add(1, 1);
        tmp.add(-7, 0);

        assertTrue(PolyOperations.checkEquality(r, tmp));
    }

    @Test
    public void subtract() {
        initPolynomials();
        Polynomial r = PolyOperations.subtract(p, q);
        Polynomial tmp = new Polynomial();

        tmp.add(1, 3);
        tmp.add(-2, 2);
        tmp.add(-1, 1);
        tmp.add(-1, 0);

        assertTrue(PolyOperations.checkEquality(r, tmp));
    }

    @Test
    public void multiply() {
        initPolynomials();
        Polynomial r = PolyOperations.multiply(p, q);
        Polynomial tmp = new Polynomial();

        tmp.add(1, 4);
        tmp.add(-5, 3);
        tmp.add(6, 2);
        tmp.add(-4, 1);
        tmp.add(12, 0);

        assertTrue(PolyOperations.checkEquality(r, tmp));
    }

    @Test
    public void divide() {
        initPolynomials();
        QRPair r = PolyOperations.divide(p, q);

        Polynomial tmpQ = new Polynomial();
        Polynomial tmpR = new Polynomial();

        tmpQ.add(1, 2);
        tmpQ.add(1, 1);
        tmpQ.add(3, 0);


        tmpR.add(5, 0);

        assertTrue(PolyOperations.checkEquality( r.getQuotient(), tmpQ));
        assertTrue(PolyOperations.checkEquality(r.getRemainder(), tmpR));
    }

    @Test
    public void differentiate() {
        initPolynomials();
        Polynomial r = PolyOperations.differentiate(p);
        Polynomial tmp = new Polynomial();
        tmp.add(3, 2);
        tmp.add(-4, 1);

        assertTrue(PolyOperations.checkEquality(r, tmp));
    }

    @Test
    public void integrate() {
        initPolynomials();
        Polynomial r = PolyOperations.integrate(p);
        Polynomial tmp = new Polynomial();
        tmp.add(1.0/4, 4);
        tmp.add(-2.0/3, 3);
        tmp.add(-4, 1);

        assertTrue(PolyOperations.checkEquality(r, tmp));
    }

    private void initPolynomials() {
        p = new Polynomial();
        q = new Polynomial();
        // p = x^3 - 2x^2 - 4
        p.add(1, 3);
        p.add(-2, 2);
        p.add(-4, 0);

        // q = x - 3
        q.add(1, 1);
        q.add(-3, 0);
    }
}