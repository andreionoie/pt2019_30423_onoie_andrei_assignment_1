package test;

import PolyLogic.PolyUtilities;
import PolyLogic.Polynomial;
import PolyLogic.Term;
import org.junit.Test;

import static org.junit.Assert.*;

public class PolyUtilitiesTest {
    private double[] coeff = new double[] {4, 2, -7.3};
    private int[] power = new int[] {3, 1, 0};

    @Test
    public void copyPolynomial() {
        Polynomial p = new Polynomial();

        // p = 4x^3 + 2x - 7.3
        p.add(coeff[0], power[0]);
        p.add(coeff[1], power[1]);
        p.add(coeff[2], power[2]);

        Polynomial q = PolyUtilities.copyPolynomial(p);
        q.getLeadTerm().addToPower(3);

        // checking if we did a deep copy, not a reference copy
        assertTrue(p.getLeadTerm().compareTo(q.getLeadTerm()) != 0);
        assertTrue(p.getTerms().get(1).compareTo(q.getTerms().get(1)) == 0);
        assertTrue(p.getTerms().get(2).compareTo(q.getTerms().get(2)) == 0);
    }

    @Test
    public void divideTerms() {
        Term t1 = new Term(4.3, 5);
        Term t2 = new Term(1.25, 3);

        // t3(x) = (4.3x^5)/(1.25x^3)
        Term t3 = PolyUtilities.divideTerms(t1, t2);

        assertTrue(t3.getCoefficient() == 4.3/1.25);
        assertTrue(t3.getPower() == 5 - 3);
    }

    @Test
    public void validatePolynomial() {
        String s1 = "abcabca!@#^%&*^*ADSxzcxzc9";
        String s2 = "+x^3 - 7.32x^9 -0.3x";

        assertFalse(PolyUtilities.validatePolynomial(s1));
        assertTrue(PolyUtilities.validatePolynomial(s2));
    }

    @Test
    public void parsePolynomial() {
        String s = "4x^3 + 2x^1 - 7.3x^0";

        Polynomial p = new Polynomial();

        // p = 4x^3 + 2x - 7.3
        p.add(coeff[0], power[0]);
        p.add(coeff[1], power[1]);
        p.add(coeff[2], power[2]);

        Polynomial q = PolyUtilities.parsePolynomial(s);

        assertTrue(p.getTerms().get(0).compareTo(q.getTerms().get(0)) == 0);
        assertTrue(p.getTerms().get(1).compareTo(q.getTerms().get(1)) == 0);
        assertTrue(p.getTerms().get(2).compareTo(q.getTerms().get(2)) == 0);
    }
}