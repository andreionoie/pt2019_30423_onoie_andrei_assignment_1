package test;

import PolyLogic.Term;
import org.junit.Test;

import static org.junit.Assert.*;

public class TermTest {
    @Test
    public void testConstructor() {
        Term t = new Term(20, 40);

        assertEquals(t.getCoefficient(), 20.0, 0.001);
        assertEquals(t.getPower(), 40);
    }

    @Test
    public void testCompare() {
        Term t1 = new Term(1, 100);
        Term t2 = new Term(100, 1);
        Term t3 = new Term(2, 100);

        assertTrue(t1.compareTo(t2) < 0);
        assertTrue(t1.compareTo(t3) == 0);
        assertTrue(t2.compareTo(t3) > 0);
    }

    @Test
    public void testOperations() {
        Term t = new Term(2.3, 4);

        t.addToCoefficient(3.4);
        t.addToPower(5);

        assertEquals(t.getCoefficient(), 2.3 + 3.4, 0.001);
        assertEquals(t.getPower(), 4 + 5);

        t.setCoefficient(-7.03);
        assertTrue(t.hasNegativeCoefficient());
    }
}