package test;

import PolyLogic.Polynomial;
import PolyLogic.Term;
import org.junit.Test;

import static org.junit.Assert.*;

public class PolynomialTest {
    private double[] coeff = new double[] {1, 2, -7.3};
    private int[] power = new int[] {3, 1, 0};

    @Test
    public void testConstructor() {
        Polynomial p = new Polynomial();

        // p = x^3 + 2x - 7.3
        p.add(coeff[0], power[0]);
        p.add(coeff[1], power[1]);
        p.add(coeff[2], power[2]);

        int i=0;

        for (Term t : p.getTerms()) {
            assertTrue(t.getCoefficient() == coeff[i]);
            assertTrue(t.getPower() == power[i]);
            i++;
        }

        Polynomial q = new Polynomial();

        // q = x^3 + 2x - 7.3, but entered in reverse order
        q.add(coeff[2], power[2]);
        q.add(coeff[1], power[1]);
        q.add(coeff[0], power[0]);

        i=0;

        for (Term t : q.getTerms()) {
            assertTrue(t.getCoefficient() == coeff[i]);
            assertTrue(t.getPower() == power[i]);
            i++;
        }

    }

    @Test
    public void testHelperMethods() {
        Polynomial p = new Polynomial();

        assertTrue(p.isEmpty());
        // p = x^3 + 2x - 7.3
        p.add(coeff[0], power[0]);
        p.add(coeff[1], power[1]);
        p.add(coeff[2], power[2]);
        assertFalse(p.isEmpty());
        assertTrue(p.degree() == 3);

        assertTrue(p.getLeadTerm().getCoefficient() == coeff[0]);
        assertTrue(p.getLeadTerm().getPower() == power[0]);

        assertTrue(p.getNbOfTerms() == 3);
    }
}