package PolyLogic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Polynomial {
    private List<Term> terms;

    public Polynomial() {
        terms = new ArrayList<Term>();
    }

    public Polynomial(double coefficient, int power) {
        this();
        add(coefficient, power);
    }

    public void add(double coefficient, int power) {
        if (coefficient == 0)
            return;

        terms.add(new Term(coefficient, power));
        Collections.sort(terms);
    }

    public List<Term> getTerms() {
        return terms;
    }

    public int getNbOfTerms() { return terms.size(); };

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder("");

        for (Term m : terms) {
            if (m.hasNegativeCoefficient())
                out.append("- ");
            else
                out.append("+ ");

            out.append(m.toString() + " ");
        }

        return out.toString();
    }

    public boolean isEmpty() {
        return terms.isEmpty();
    }

    public int degree() {
        if (terms.isEmpty())
            return -1;

        return terms.get(0).getPower();
    }

    public Term getLeadTerm() {
        if (terms.isEmpty())
            return null;

        return terms.get(0);
    }
}
