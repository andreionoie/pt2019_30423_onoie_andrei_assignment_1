package PolyLogic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PolyUtilities {
    private static final String POLY_REGEX = "([+-]?[^-+]+)";

    public static Polynomial copyPolynomial(Polynomial src) {
        Polynomial dest = new Polynomial();

        for (Term m : src.getTerms())
            dest.add(m.getCoefficient(), m.getPower());

        return dest;
    }

    public static Term divideTerms(Term t1, Term t2) {
        return new Term(t1.getCoefficient() / t2.getCoefficient(),
                t1.getPower() - t2.getPower());
    }

    public static boolean validatePolynomial(String p) {
        // string only contains characters x, X, dot, plus, caret, whitespace and digits
        return p.matches("^[Xx0-9.+\\-^ ]+$");
    }

    public static Polynomial parsePolynomial(String s) {
        Polynomial p = new Polynomial();


        double coeff = 1;
        int exp = 0;

        Pattern pattern = Pattern.compile(POLY_REGEX);
        Matcher matcher = pattern.matcher(s);

        while (matcher.find()) {
            String coeffStr = "";
            String tmpTerm = matcher.group(1);
            coeff = 1;

            if (tmpTerm.charAt(0) == '-')
                coeff = -1;

            // turn string term into instance of PolyLogic.Term
            for (int i=1; i < tmpTerm.length(); i++) {
                if (tmpTerm.charAt(i) == ' ') continue;

                if (tmpTerm.toLowerCase().charAt(i) == 'x') {
                    if (!coeffStr.isBlank())
                        coeff *= Double.parseDouble(coeffStr.trim());

                    coeffStr = "";
                    continue;
                }

                coeffStr += tmpTerm.charAt(i) + "";

                if (tmpTerm.charAt(i) == '^') {
                    exp = Integer.parseInt(tmpTerm.substring(i+1).trim());
                    break;
                }

            }

            p.add(coeff, exp);
        }

        return p;
    }
}
