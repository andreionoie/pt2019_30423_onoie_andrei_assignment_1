package PolyLogic;

public class Term implements Comparable<Term> {

    private double coefficient;
    private int power;

    public Term(double coefficient, int power) {
        this.coefficient = coefficient;
        this.power = power;
    }

    @Override
    public String toString() {
        double tmpCoeff;
        String coeff;

        if (coefficient % 1 == 0)
            coeff = (Math.abs(coefficient) == 1) ? "" :(int) Math.abs(coefficient) + "";
        else
            coeff = Math.round(Math.abs(coefficient)*100.0)/100.0 + "";

        return coeff + "x^" + power;

    }

    public boolean hasNegativeCoefficient() {
        return coefficient < 0;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public int getPower() {
        return power;
    }

    public void addToCoefficient(double value) {
        coefficient += value;
    }

    public void addToPower(int value) {
        power += value;
    }


    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    // order by reverse order of exponent
    public int compareTo(Term o) {
        return - (this.power - o.getPower());
    }
}
