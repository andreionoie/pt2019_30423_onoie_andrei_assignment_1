package PolyLogic;

public class QRPair {
    private final Polynomial quotient, remainder;

    public QRPair(Polynomial quotient, Polynomial remainder) {
        this.quotient = quotient;
        this.remainder = remainder;
    }

    public Polynomial getQuotient() {
        return quotient;
    }

    public Polynomial getRemainder() {
        return remainder;
    }
}
