package PolyLogic;

import java.util.ArrayList;
import java.util.List;

public class PolyOperations {
    public static Polynomial add(Polynomial p1, Polynomial p2) {
        Polynomial out = PolyUtilities.copyPolynomial(p1);

        for (Term m : p2.getTerms()) {     // m in p2
            Term c = null;

            for (Term n : out.getTerms()) { // n in out
                if (m.getPower() == n.getPower()) {
                    c = n;
                    break;
                }
            }

            if (c != null) {
                c.addToCoefficient(m.getCoefficient());

                if (c.getCoefficient() == 0) {
                    out.getTerms().remove(c);
                }
            } else {
                out.add(m.getCoefficient(), m.getPower());
            }
        }

        return out;
    }

    public static Polynomial subtract(Polynomial p1, Polynomial p2) {
        Polynomial out = PolyUtilities.copyPolynomial(p1);

        for (Term m : p2.getTerms()) {     // m in p2
            Term c = null;

            for (Term n : out.getTerms()) { // n in out
                if (m.getPower() == n.getPower()) {
                    c = n;
                    break;
                }
            }

            if (c != null) {
                c.addToCoefficient(-m.getCoefficient());

                if (c.getCoefficient() == 0) {
                    out.getTerms().remove(c);
                }
            } else {
                out.add(-m.getCoefficient(), m.getPower());
            }
        }

        return out;
    }

    public static Polynomial mulByTerm(Polynomial p, Term t) {
        Polynomial out = PolyUtilities.copyPolynomial(p);

        for (Term m : out.getTerms()) {
            m.setCoefficient(m.getCoefficient() * t.getCoefficient());
            m.addToPower(t.getPower());
        }

        return out;
    }

    public static Polynomial multiply(Polynomial p1, Polynomial p2) {
        List<Polynomial> terms = new ArrayList<>();
        Polynomial out = new Polynomial();

        for (Term m : p2.getTerms()) {
            terms.add(mulByTerm(p1, m));
        }

        for (Polynomial p : terms) {
            out = add(out, p);
        }

        return out;
    }

    public static QRPair divide(Polynomial p1, Polynomial p2) {
        Polynomial quot, rem, tmp;
        quot = new Polynomial(0, 0);
        rem = PolyUtilities.copyPolynomial(p1);

        while (!rem.isEmpty() && rem.degree() >= p2.degree()) {
            Term t = PolyUtilities.divideTerms(rem.getLeadTerm(), p2.getLeadTerm());
            tmp = new Polynomial(t.getCoefficient(), t.getPower());

            quot = add(quot, tmp);
            rem = subtract(rem, multiply(p2, tmp));
        }

        return new QRPair(quot, rem);
    }

    public static Polynomial differentiate(Polynomial p) {
        Polynomial out = PolyUtilities.copyPolynomial(p);
        Term toBeDeleted = null;

        for (Term t : out.getTerms()) {
            if (t.getPower() == 0) {
                toBeDeleted = t;
            } else {
                t.setCoefficient(t.getCoefficient() * t.getPower());
                t.addToPower(-1);
            }
        }

        if (toBeDeleted != null)
            out.getTerms().remove(toBeDeleted);

        return out;
    }

    public static Polynomial integrate(Polynomial p) {
        Polynomial out = PolyUtilities.copyPolynomial(p);

        for (Term t : out.getTerms()) {
            t.setCoefficient(t.getCoefficient() / (t.getPower() + 1));
            t.addToPower(1);
        }

        return out;
    }

    public static boolean checkEquality(Polynomial p1, Polynomial p2) {
        int i=0;

        for (Term t : p1.getTerms()) {
            if (t.getCoefficient() != p2.getTerms().get(i).getCoefficient() ||
                t.getPower() != p2.getTerms().get(i).getPower())
                return false;
            i++;

            if (i >= p2.getNbOfTerms())
                break;
        }

        return true;
    }
}
