package PolyUI;

import PolyLogic.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class PolynomialsFrame extends JFrame {
    private JTextField p1, p2, resultField;
    private JComboBox<String> operations;
    private JButton computeButton;
    private JLabel operationLabel;
    private static String longString = "                                                                                                                                  ";
    private static String[] supportedOperations = {"Addition", "Subtraction", "Multiply", "Divide", "Differentiate", "Integrate" };
    public PolynomialsFrame() {
        setTitle("Polynomials");
        setSize(400, 400);
        setLocation(new Point(300, 200));
        setResizable(false);


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        initComponents();
        initActionListeners();
        //pack();

        setVisible(true);
    }

    private void initComponents() {
        JPanel windowPane = new JPanel(new GridLayout(8, 1));

        p1 = new JTextField(longString, 30);
        p2 = new JTextField(longString, 30);
        p1.setText("x^3 - 2x^2 - 4x^0"); p2.setText("x^1 - 3x^0");
        resultField = new JTextField(longString, 30);
        resultField.setText("......");
        resultField.setEditable(false);
        resultField.setHorizontalAlignment(JTextField.CENTER);
        resultField.setBackground(Color.WHITE);
        resultField.setFont(resultField.getFont().deriveFont(Font.BOLD, 14f));

        operations = new JComboBox<String>(supportedOperations);

        computeButton = new JButton("Compute");
        operationLabel = new JLabel();
        operationLabel.setHorizontalAlignment(JLabel.CENTER);

        p1.setSize(200, 20);

        JPanel firstTermPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        firstTermPanel.add(new JLabel("P1(x) = "));
        firstTermPanel.add(p1);

        JPanel secondTermPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        secondTermPanel.add(new JLabel("P2(x) = "));
        secondTermPanel.add(p2);

        JPanel selOperationPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        selOperationPanel.add(operations);
        selOperationPanel.add(computeButton);

        windowPane.add(firstTermPanel);
        windowPane.add(secondTermPanel);
        windowPane.add(selOperationPanel);
        windowPane.add(new JLabel());
        windowPane.add(new JLabel());
        windowPane.add(operationLabel);
        windowPane.add(new JLabel("=", JLabel.CENTER));
        windowPane.add(resultField);

        getContentPane().add(windowPane);
    }

    private void initActionListeners() {
        computeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (validateInput()) {
                    String operation = (String) operations.getSelectedItem();
                    updateOperationLabel(operation);
                    computeOperation(operation);
                }

            }
        });
    }

    private void updateOperationLabel(String operation) {
        String opchar;

        switch (operation) {
            case "Addition":    opchar = "+"; break;
            case "Subtraction": opchar = "-"; break;
            case "Multiply":    opchar = "x"; break;
            default:            opchar = "?";
        }

        if (opchar.equals("?")) {
            if (operation == "Differentiate") {
                operationLabel.setText("(" + p1.getText() + ")'");
            } else if (operation == "Integrate") {
                operationLabel.setText("\u222B(" + p1.getText() + ") dx");
            } else if (operation == "Divide") {
                operationLabel.setText(p1.getText());
            }
        } else {
            operationLabel.setText("(" + p1.getText() + ") " + opchar + " (" + p2.getText() + ")");
        }
    }

    private void computeOperation(String operation) {
        String result = "......";
        Polynomial poly1, poly2;
        poly1 = PolyUtilities.parsePolynomial(p1.getText());
        poly2 = PolyUtilities.parsePolynomial(p2.getText());

        switch (operation) {
            case "Addition":
                result = PolyOperations.add(poly1, poly2).toString();
                break;
            case "Subtraction":
                result = PolyOperations.subtract(poly1, poly2).toString();
                break;
            case "Multiply":
                result = PolyOperations.multiply(poly1, poly2).toString();
                break;
            case "Divide":
                QRPair divResult = PolyOperations.divide(poly1, poly2);
                result = "(" + poly2 + ")(" + divResult.getQuotient() + ") " + divResult.getRemainder();
                break;
            case "Differentiate":
                result = PolyOperations.differentiate(poly1).toString();
                break;
            case "Integrate":
                result = PolyOperations.integrate(poly1).toString();
                break;
        }

        resultField.setText(result);
    }

    private boolean validateInput() {
        boolean p1Valid, p2Valid;
        p1Valid = PolyUtilities.validatePolynomial(p1.getText());
        p2Valid = PolyUtilities.validatePolynomial(p2.getText());

        if (p1Valid && p2Valid) {
            return true;
        } else {
            JOptionPane.showMessageDialog(this.getContentPane(), "P1 and/or P2 are invalid!", "Input error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
